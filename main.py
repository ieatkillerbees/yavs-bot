import os
from os import path
import logging

import discord
from discord.ext.commands.bot import Bot


logging.basicConfig(format='%(asctime)s|%(levelname)s|%(message)s', level=logging.INFO)

class CuddleBot(Bot):
    """This is similar to :class:`.Bot` except that it also listens to bots"""

    async def process_commands(self, message):
        ctx = await self.get_context(message)
        await self.invoke(ctx)


extensions = []
for r, _, f in os.walk('cogs'):
    for name in f:
        if path.isfile(path.join(r, name)) and name.endswith('.py'):
            extensions.append(path.join(r, name).replace('/', '.')[0:-3])

intents = discord.Intents(messages=True, guilds=True, reactions=True)

bot = CuddleBot(command_prefix='>', intents=intents)


@bot.event
async def on_ready() -> None:
    logging.info('Logged in as')
    logging.info(bot.user.name)
    logging.info(bot.user.id)
    logging.info('------')
    await bot.change_presence(activity=discord.Activity(name='>help', type=discord.ActivityType.listening))


if __name__ == "__main__":
    for extension in extensions:
        try:
            bot.load_extension(f'{extension}')
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            logging.warning(f'Failed to load extension {extension}\n{exc}')

    bot.run(os.environ.get('BOT_TOKEN'))
