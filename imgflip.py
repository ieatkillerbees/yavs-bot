import requests
import logging

class Client:
    def __init__(self, username='', password='', root_url='https://api.imgflip.com/'):
        super().__init__()
        self.username = username
        self.password = password
        self.urls = {
            'get_memes': root_url + 'get_memes',
            'caption_image': root_url + 'caption_image',
        }
        self.memes = []

    def get_memes(self, update=False):
        if len(self.memes) == 0 or update:
            r = requests.get(self.urls['get_memes'])
            data = r.json()
            return data['data']['memes']
        return self.memes

    def caption_image(self, identifier, captions: [str]=None):
        if identifier is None:
            logging.warning('[imgflip] Error')

        data = {
            'template_id': identifier,
            'username': self.username,
            'password': self.password,
        }

        if len(captions) == 2:
            data['text0'] = captions[0]
            data['text1'] = captions[1]
        else:
            for index, caption in enumerate(captions):
                data[f'boxes[{index}][text]'] = caption
                data[f'boxes[{index}][color]'] = "#ffffff"
                data[f'boxes[{index}][outline_color]'] = "#000000"

        r = requests.post(self.urls['caption_image'], data=data)
        json = r.json()
        return json['data']['url']