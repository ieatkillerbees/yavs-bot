import re
import logging

from discord.ext import commands

from utils import get_guild_settings, save_guild_settings

def syllables_in_sentence(sentence):
    syllables = 0
    for word in sentence.split(' '):
        syllables += count_syllables(word)
    return syllables

# From: https://datascience.stackexchange.com/a/89312
VOWEL_RUNS = re.compile("[aeiouy]+", flags=re.I)
EXCEPTIONS = re.compile(
    # fixes trailing e issues:
    # smite, scared
    "[^aeiou]e[sd]?$|"
    # fixes adverbs:
    # nicely
    + "[^e]ely$",
    flags=re.I
)
ADDITIONAL = re.compile(
    # fixes incorrect subtractions from exceptions:
    # smile, scarred, raises, fated
    "[^aeioulr][lr]e[sd]?$|[csgz]es$|[td]ed$|"
    # fixes miscellaneous issues:
    # flying, piano, video, prism, fire, evaluate
    + ".y[aeiou]|ia(?!n$)|eo|ism$|[^aeiou]ire$|[^gq]ua",
    flags=re.I
)

def count_syllables(word):
    vowel_runs = len(VOWEL_RUNS.findall(word))
    exceptions = len(EXCEPTIONS.findall(word))
    additional = len(ADDITIONAL.findall(word))
    return max(1, vowel_runs - exceptions + additional)

def write_haiku(content):
    words = content.split(' ')
    i = 0
    paragraph = 0
    haiku = ['', '', '']
    is_haiku = True

    for word in words:
        haiku[paragraph] += word + ' '
        i += count_syllables(word)

        if paragraph == 0:
            if i == 5:
                paragraph = 1
                i = 0
            if i > 5:
                is_haiku = False

        if paragraph == 1:
            if i == 7:
                paragraph = 2
                i = 0
            if i > 7:
                is_haiku = False
    if is_haiku:
        return "\n".join(haiku)
    else:
        return None

HAIKU_CHANNELS_KEY = 'haiku_channels'

class Haiku(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message) -> None:
        if message.author == self.bot.user:
            return
        guild_settings = get_guild_settings(message.guild.id)
        if HAIKU_CHANNELS_KEY not in guild_settings:
            return
        if message.channel.id not in guild_settings[HAIKU_CHANNELS_KEY]:
            return
        content = message.content.replace("\n", " ")
        if syllables_in_sentence(content) == 17:
            haiku = write_haiku(content)
            if haiku is not None:
                logging.info("Found a haiku")
                await message.channel.send(content="```\n" + haiku + "\n```")

    @commands.command(
        name="allow-haiku",
        help="Allows cuddle to detect haikus in the channel",
        brief="Allows cuddle to detect haikus in the channell"
    )
    @commands.has_guild_permissions(manage_guild=True)
    async def allow_haiku(self, ctx):
        guild_settings = get_guild_settings(ctx.guild.id)
        if HAIKU_CHANNELS_KEY not in guild_settings:
            guild_settings[HAIKU_CHANNELS_KEY] = []
        if ctx.channel.id in guild_settings[HAIKU_CHANNELS_KEY]:
            guild_settings[HAIKU_CHANNELS_KEY].remove(ctx.channel.id)
            await ctx.channel.send('I will now no longer detect haikus in this chnanel')
        else:
            guild_settings[HAIKU_CHANNELS_KEY].append(ctx.channel.id)
            await ctx.channel.send('I will now detect haikus in this channel')
        save_guild_settings(ctx.guild.id, guild_settings)

def setup(bot) -> None:
    bot.add_cog(Haiku(bot))
