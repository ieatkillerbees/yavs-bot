from discord import HTTPException
from discord.ext import commands

from utils import get_guild_settings, save_guild_settings

message_link = "https://discordapp.com/channels/{}/{}/{}"

EMOJI_NAMES = ["pin", "📌"]


class Pinning(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if payload.guild_id is None:
            return  # Reaction is on a private message
        if payload.emoji.name in EMOJI_NAMES:
            channel = self.bot.get_channel(payload.channel_id)
            message = await channel.fetch_message(payload.message_id)
            if message.pinned:
                return
            reaction = next(x for x in message.reactions if (isinstance(x.emoji, str) and str(x.emoji) in EMOJI_NAMES) or (not isinstance(x.emoji, str)and x.emoji.name in EMOJI_NAMES))
            if reaction.count >= 5:
                try:
                    await message.pin()
                except HTTPException:  # This most likely means we reached max pins
                    pins = await channel.pins()
                    oldest_pin = pins.pop()
                    guild_settings = get_guild_settings(payload.guild_id)
                    if "old-pins" not in guild_settings:
                        return
                    old_pins_channel = self.bot.get_channel(guild_settings["old-pins"])
                    link = message_link.format(payload.guild_id, channel.id, oldest_pin.id)
                    await old_pins_channel.send(f"<#{channel.id}>: {link}")
                    await oldest_pin.unpin()
                    await message.pin()


def setup(bot):
    bot.add_cog(Pinning(bot))
