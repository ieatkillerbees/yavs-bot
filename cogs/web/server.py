from aiohttp import web
import jinja2
import aiohttp_jinja2
import asyncio
import pathlib
import os
from discord.ext import commands
from utils import get_guild_settings, save_guild_settings
from cogs.emote_stats import EMOJI_STATS_KEY
from cogs.spoiler_events import SPOILER_CHANNELS_KEY

FACT_OR_CRAP_KEY = "fact_or_crap"


class WebServer(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.site = None
        self.base_path = pathlib.Path(__file__).parent.absolute()
        self.main_guild = os.environ.get("MAIN_GUILD_ID")
        self.guild = None
        self.secret_key = os.environ.get("WEB_SECRET")

    async def webserver(self):
        app = web.Application()
        aiohttp_jinja2.setup(
            app, loader=jinja2.FileSystemLoader(os.path.join(pathlib.Path(__file__).parent.absolute(), "templates"))
        )
        app.router.add_get('/', self.index_handler)
        app.router.add_get('/fact-or-crap', self.show_fact_or_crap)
        app.router.add_post('/fact-or-crap', self.save_fact_or_crap)
        runner = web.AppRunner(app)
        await runner.setup()
        self.site = web.TCPSite(runner, None, 3000)
        await self.bot.wait_until_ready()
        self.guild = await self.bot.fetch_guild(self.main_guild)
        await self.site.start()

    async def index_handler(self, request):
        spoiler_channels = []
        guild_settings = get_guild_settings(self.main_guild)
        if SPOILER_CHANNELS_KEY in guild_settings:
            for channel in guild_settings[SPOILER_CHANNELS_KEY]:
                spoiler_channels.append(await self.bot.fetch_channel(channel))
        anime_channel = None
        if 'anime' in guild_settings:
            anime_channel = await self.bot.fetch_channel(guild_settings['anime'])
        context = {
            'spoiler_channels': spoiler_channels,
            'anime': anime_channel,
            'emoji_stats': guild_settings[EMOJI_STATS_KEY] if EMOJI_STATS_KEY in guild_settings else {}
        }
        return aiohttp_jinja2.render_template("index.html", request, context=context)

    async def show_fact_or_crap(self, request: web.Request):
        guild_settings = get_guild_settings(self.main_guild)
        if FACT_OR_CRAP_KEY not in guild_settings:
            guild_settings[FACT_OR_CRAP_KEY] = {}

        users = {}
        for date in guild_settings[FACT_OR_CRAP_KEY]:
            if "answer" not in guild_settings[FACT_OR_CRAP_KEY][date]:
                continue
            for user_id in guild_settings[FACT_OR_CRAP_KEY][date]:
                if user_id == "answer":
                    continue
                if user_id not in users:
                    try:
                        member = self.guild.get_member(user_id)
                        if member is None:
                            member = await self.guild.fetch_member(user_id)
                            users[user_id] = {
                                'name': f"{member.name}#{member.discriminator}",
                                'amount': 0
                            }
                    except:
                        users[user_id] = {
                            'name': f"UNKNOWN",
                            'amount': 0
                        }
                if guild_settings[FACT_OR_CRAP_KEY][date][user_id] == guild_settings[FACT_OR_CRAP_KEY][date]["answer"]:
                    users[user_id]["amount"] += 1
        context = {
            'users': sorted(users.values(), key=lambda u: u["amount"], reverse=True)
        }
        return aiohttp_jinja2.render_template("leaderboard.html", request, context=context)

    async def save_fact_or_crap(self, request: web.Request):
        form = await request.post()
        if "key" not in form or form.get("key") != self.secret_key:
            raise web.HTTPForbidden(body="incorrect secret")

        guild_settings = get_guild_settings(self.main_guild)
        if FACT_OR_CRAP_KEY not in guild_settings:
            guild_settings[FACT_OR_CRAP_KEY] = {}
        date = form.get("date")
        if date not in guild_settings[FACT_OR_CRAP_KEY]:
            guild_settings[FACT_OR_CRAP_KEY][date] = {}
        guild_settings[FACT_OR_CRAP_KEY][date]["answer"] = form.get("fact") == 'true'
        save_guild_settings(self.main_guild, guild_settings)
        return web.HTTPOk(body="ok")

    def __unload(self):
        asyncio.ensure_future(self.site.stop())


def setup(bot):
    web = WebServer(bot)
    bot.add_cog(web)
    bot.loop.create_task(web.webserver())
