import io

import discord
import requests
from discord.ext import commands
from random import choice
from asyncurban import UrbanDictionary
import asyncio
import os
import imgflip

from utils import is_spoiler_channel

NEXT_BUTTON_EMOJI = '⏭️'
PREV_BUTTON_EMOJI = '⏮️'
DELETE_EMOJI = '❌'


class Fun(commands.Cog):
    ball = [
        "As I see it, yes",
        "It is certain",
        "It is decidedly so",
        "Most likely",
        "Outlook good",
        "Signs point to yes",
        "Without a doubt",
        "Yes",
        "Yes – definitely",
        "You may rely on it",
        "Reply hazy, try again",
        "Ask again later",
        "Better not tell you now",
        "Cannot predict now",
        "Concentrate and ask again",
        "Don't count on it",
        "My reply is no",
        "My sources say no",
        "Outlook not so good",
        "Very doubtful"
    ]

    def __init__(self, bot):
        self.bot = bot
        self.urban = UrbanDictionary(loop=bot.loop)
        self.imgflip = imgflip.Client(username=os.environ.get("IMGFLIP_USER", ""), password=os.environ.get("IMGFLIP_PASS"))

    @commands.command(name="8", aliases=["8ball"])
    async def _8ball(self, ctx, *, question: str):
        """Ask 8 ball a question.
        Question must end with a question mark.
        """
        if question.endswith("?") and question != "?":
            await ctx.send("`" + (choice(self.ball)) + "`")
        else:
            await ctx.send("That doesn't look like a question.")

    @commands.command(name="imgflip", aliases=["meme"])
    async def _imgflip(self, ctx, *captions):
        if len(captions) == 1:
            captions += (" ",)
        templates = self.imgflip.get_memes()
        templates = list(filter(lambda x: (int(x['box_count']) >= len(captions)), templates))
        template = choice(templates)
        meme = self.imgflip.caption_image(template['id'], captions)

        spoiler = False
        if is_spoiler_channel(ctx):
            spoiler = True

        r = requests.get(meme, allow_redirects=True)
        big_emoji = io.BytesIO(r.content)
        file = discord.File(big_emoji, "meme.jpg", spoiler=spoiler)
        await ctx.send("", file=file)

    @commands.command(
        name='urban',
        aliases=['ud', 'urbandictionary'],
        no_pm=True
    )
    async def urban(self, ctx, *text):
        current = 0
        try:
            words = await self.urban.search(' '.join(text), limit=10)
            word = words[current]
            embed = discord.Embed(
                title="Definition for " + word.word,
                description=str(word.definition)
            )
            if word.example:
                embed.add_field(name="Example", value=word.example, inline=False)
            embed.add_field(name="Upvotes", value=str(word.votes['up']), inline=True)
            embed.add_field(name="Downvotes", value=str(word.votes['down']), inline=True)
            urban_msg = await ctx.send(embed=embed)
            await urban_msg.add_reaction(PREV_BUTTON_EMOJI)
            await urban_msg.add_reaction(NEXT_BUTTON_EMOJI)
            await urban_msg.add_reaction(DELETE_EMOJI)

            await self.urban_wait_for(ctx.message.author, urban_msg, words, current)
        except:
            await ctx.send(f"Unable to find a definition for *{' '.join(text)}*")

    async def urban_wait_for(self, author, urban_msg, words, current):
        def checkUp(reaction, user):
            return user == author and (str(reaction.emoji) == PREV_BUTTON_EMOJI or str(
                reaction.emoji) == NEXT_BUTTON_EMOJI or str(
                reaction.emoji) == DELETE_EMOJI)

        try:
            reaction, user = await self.bot.wait_for('reaction_add', timeout=30.0, check=checkUp)
        except asyncio.TimeoutError:
            """Do nothing?"""
        else:
            if reaction.emoji == NEXT_BUTTON_EMOJI:
                if current < len(words):
                    current += 1
            elif reaction.emoji == PREV_BUTTON_EMOJI:
                if current != 0:
                    current -= 1
            elif reaction.emoji == DELETE_EMOJI:
                await urban_msg.delete()
                return

            word = words[current]
            embed = discord.Embed(
                title="Definition for " + word.word,
                description=str(word.definition)
            )
            if word.example:
                embed.add_field(name="Example", value=word.example, inline=False)
            embed.add_field(name="Upvotes", value=str(word.votes['up']), inline=True)
            embed.add_field(name="Downvotes", value=str(word.votes['down']), inline=True)
            await urban_msg.edit(embed=embed)

            await reaction.remove(user)
            await self.urban_wait_for(author, urban_msg, words, current)


def setup(bot):
    bot.add_cog(Fun(bot))
