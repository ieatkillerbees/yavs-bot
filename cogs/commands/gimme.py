from typing import Optional

from discord import Emoji, Guild
from discord.ext import commands


class Gimme(commands.Cog):
    @commands.command(
        name='gimme',
        help='Echo the named server emoji',
    )
    async def gimme(self, ctx, name, *args):
        """Send the named emoji as a mention if it exists in the server, otherwise do nothing."""
        if args and (name, args[0]) == ('gimme', 'gimme'):  # easter egg!
            return await ctx.send('https://www.youtube.com/watch?v=XEjLoHdbVeE')

        emoji = self.find_emoji(ctx.message.guild, name)
        if emoji:
            await ctx.send(str(emoji))

    @staticmethod
    def find_emoji(guild: Guild, name: str) -> Optional[Emoji]:
        """Find the emoji with the given name in the given server, or None if it does not exist."""
        return next((e for e in guild.emojis if e.name == name), None)


def setup(bot):
    bot.add_cog(Gimme())
