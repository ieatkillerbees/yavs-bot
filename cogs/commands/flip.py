import random

from discord.ext import commands

class Flip(commands.Cog):
    flips = [
        '(╯°□°）╯︵ ┻━┻',
        '(┛◉Д◉)┛彡┻━┻',
        '(ﾉ≧∇≦)ﾉ ﾐ ┸━┸',
        '(ノಠ益ಠ)ノ彡┻━┻',
        '(╯ರ ~ ರ）╯︵ ┻━┻',
        '(┛ಸ_ಸ)┛彡┻━┻',
        '(ﾉ´･ω･)ﾉ ﾐ ┸━┸',
        '(ノಥ,_｣ಥ)ノ彡┻━┻',
        '(┛✧Д✧))┛彡┻━┻',
    ]

    @commands.command(
        name='flip',
        help='Flip a f&*#ing table!',
    )
    async def flip(self, ctx, *text):
        """Flip a random table"""
        await ctx.send(random.choice(self.flips))

def setup(bot):
    bot.add_cog(Flip())
