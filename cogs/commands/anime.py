from dataclasses import dataclass

import requests
import json

from typing import Tuple

from utils import is_valid_channel

from discord.ext.commands import MissingRequiredArgument, BadArgument, CommandOnCooldown
from discord.ext import commands
from discord import Embed


@dataclass
class AnimeInfo:
    Title: str
    Type: str
    Episodes: str
    Airing: bool
    Score: float
    Synopsis: str
    Image_URL: str
    URL: str


# Know more about anime
class Anime(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name='anime',
        aliases=['animeinfo', 'ai', 'mal', 'myanimelist'],
        help="Fetch information about an anime from myanimelist",
        brief="Show info about anime"
    )
    async def anime(self, ctx, *, term: str) -> None:
        if not is_valid_channel(ctx, "anime"):
            return
        query = await self.get_anime_info_by_name(term)
        if query:
            await ctx.send(embed=Embed(color=0x2F3136, title=f"Anime Information for {query.Title} - {query.Type}",
                                       description=f"ᕙ(⇀‸↼‵‵)ᕗ **{ctx.author.mention}** I've found your anime!")
                           .set_thumbnail(url=query.Image_URL)
                           .add_field(name="**Information**",
                                      value=f"MAL Score: **{query.Score}**\nEpisodes: **{query.Episodes}**\nAiring: **{'Yes' if query.Airing else 'No'}**")
                           .add_field(name=f"**Synopsis**", value=f"{query.Synopsis}\n[See it on MAL]({query.URL})"))
        else:
            await ctx.send("It's so embarrassing.. I can't find this anime..", delete_after=5.0)

    @commands.command(
        name='animequote',
        aliases=["aq"],
        help="Shows a random anime quote",
        brief="Random anime quote"
    )
    async def animequote(self, ctx) -> None:
        if not is_valid_channel(ctx, "anime"):
            return
        q, c, a = self.get_random_anime_quote()
        await ctx.send(embed=Embed(title="⊹　 ✺ * ·　",
                                   color=0x2F3136,
                                   description="« {} »\n— {} - {}".format(q, c, a)))

    # Retrieving anime quote
    def get_random_anime_quote(self) -> Tuple[any, any, any]:
        response = requests.get("https://animechanapi.xyz/api/quotes/random")
        results = json.loads(response.text)
        return results["data"][0]["quote"], results["data"][0]["character"], results["data"][0]["anime"]

    @anime.error
    async def anime_error(self, ctx, error) -> None:
        if isinstance(error, MissingRequiredArgument):
            await ctx.send("Please specify an anime name to search for", delete_after=5.0)
        elif isinstance(error, BadArgument):
            await ctx.send("It's so embarrassing.. I can't find this anime..", delete_after=5.0)
        elif isinstance(error, CommandOnCooldown):
            await ctx.send("You're typing so fast, wait a moment...", delete_after=5.0)

    # Powered with Jikan API for MAL
    async def get_anime_info_by_name(self, name: str) -> AnimeInfo:
        response = requests.get("https://api.jikan.moe/v3/search/anime?q={}".format(name))
        results = json.loads(response.text)

        return AnimeInfo(results["results"][0]["title"],
                         results["results"][0]["type"],
                         results["results"][0]["episodes"],
                         results["results"][0]["airing"],
                         results["results"][0]["score"],
                         results["results"][0]["synopsis"] if len(results["results"][0][
                                                                      "synopsis"]) < 516 else f"{results['results'][0]['synopsis'][:516]}..",
                         results["results"][0]["image_url"],
                         results["results"][0]["url"]
                         ) if len(results["results"]) > 0 else False


def setup(bot) -> None:
    bot.add_cog(Anime(bot))
