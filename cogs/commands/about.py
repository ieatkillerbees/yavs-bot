import os
import discord
from discord.ext import commands

class About(commands.Cog):
    def __init__(self, bot, commit_hash):
        self.bot = bot
        self.commit_hash = commit_hash

    @commands.command(
        name='about',
        aliases=['info'],
        help="Show information about cuddle such as the current commit.",
        brief="Shows info."
    )
    async def about(self, ctx):
        embed = discord.Embed(title="Vegan Cuddle Castle", colour=discord.Colour(0xff89e8),
                              description="The bot that's here to spread cuddles'")

        embed.set_thumbnail(
            url="https://cdn.discordapp.com/avatars/762726684897050644/4581aac0bfaf27f3f535dbfde5380855.webp?size=1024")
        embed.set_footer(text="Created for Vegan Cuddle Castle")

        embed.add_field(name="Currently deployed commit", value=f"[{self.commit_hash}](https://gitlab.com/skyexyz/yavs-bot/-/commit/{self.commit_hash})")
        embed.add_field(name="Source code", value="[Available on gitlab](https://gitlab.com/skyexyz/yavs-bot)")

        await ctx.send(embed=embed)


def setup(bot):
    commit_hash = os.environ.get("COMMIT_HASH", "unknown")
    bot.add_cog(About(bot, commit_hash))
