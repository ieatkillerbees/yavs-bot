import re

from discord import Embed, Reaction, User
from discord.ext import commands

DELETE_EMOJI = '❌'

class DeleteHAndler(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user) -> None:
        if user == self.bot.user:
            return
        if reaction.emoji != DELETE_EMOJI:
            return
        if reaction.message.author.id == self.bot.user.id:
            await reaction.message.delete()

def setup(bot) -> None:
    bot.add_cog(DeleteHandler(bot))