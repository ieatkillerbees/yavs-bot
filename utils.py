import redis
import json
import os

r = redis.Redis(host=os.environ.get("REDIS_HOST"), port=os.environ.get("REDIS_PORT"))

SPOILER_CHANNELS_KEY = 'spoiler-channels'


def get_guild_settings(guild_id) -> dict:
    guild_settings = r.get(guild_id)
    if guild_settings is None:
        guild_settings = {}
    else:
        guild_settings = json.loads(guild_settings)
    return guild_settings


def save_guild_settings(guild_id, guild_settings) -> None:
    r.set(guild_id, json.dumps(guild_settings))


def is_valid_channel(ctx, channel_name) -> bool:
    guild_settings = get_guild_settings(ctx.guild.id)
    if channel_name not in guild_settings:
        return False
    if ctx.channel.id != guild_settings[channel_name]:
        return False
    return True


def is_spoiler_channel(message):
    guild_settings = get_guild_settings(message.guild.id)
    if SPOILER_CHANNELS_KEY not in guild_settings:
        return False
    return message.channel.id in guild_settings[SPOILER_CHANNELS_KEY]
